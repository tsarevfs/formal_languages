from collections import defaultdict as ddict
import itertools
from collections import deque


def unwrap_transitions(tr):
   ans = {}
   for fr, ways in tr.items():
         ans[fr] = {}
         for ch, to in ways.items():
            ans[fr][ch] = to
   return ans

class Automata:
   def __init__(self, states, transitions, start, term):
      self._states = states
      self._transitions = transitions
      self._start = start
      self._term = term

   def __repr__(self):
      t = (str(self._transitions), str(self._start), str(self._term))
      return 'Automata{' + ', '.join(t)+ '}'

   def optional(self):
      states = {'0', '1'}
      transitions = ddict(lambda : ddict(set))
      transitions['0'][''].add('1')

      states = states.union(('0' + s for s in self._states))
      transitions['0'][''].add('0' + self._start)

      for term in self._term:
         transitions['0' + term][''].add('1')

      for fr, ways in self._transitions.items():
         for ch, to in ways.items():
            new_fr = '0' + fr
            transitions[new_fr][ch] = transitions[new_fr][ch].union(('0' + t for t in to))

      return Automata(states, unwrap_transitions(transitions), '0', {'1',})


   def closure(self):
      states = {'0', '1'}
      transitions = ddict(lambda : ddict(set))
      transitions['0'][''].add('1')
      transitions['1'][''].add('0')

      states = states.union(('0' + s for s in self._states))
      transitions['0'][''].add('0' + self._start)

      for term in self._term:
         transitions['0' + term][''].add('1')

      for fr, ways in self._transitions.items():
         for ch, to in ways.items():
            new_fr = '0' + fr
            transitions[new_fr][ch] = transitions[new_fr][ch].union(('0' + t for t in to))

      return Automata(states, unwrap_transitions(transitions), '0', {'1',})

   def concat(self, other):
      states = {'0', '1'}
      transitions = ddict(lambda : ddict(set))
      
      states = states.union(('0' + s for s in self._states))
      states = states.union(('1' + s for s in other._states))
      
      transitions['0'][''].add('0' + self._start)

      for term in self._term:
         transitions['0' + term][''].add('1' + other._start)

      for term in other._term:
         transitions['1' + term][''].add('1')

      for fr, ways in self._transitions.items():
         for ch, to in ways.items():
            new_fr = '0' + fr
            transitions[new_fr][ch] = transitions[new_fr][ch].union(('0' + t for t in to))

      for fr, ways in other._transitions.items():
         for ch, to in ways.items():
            new_fr = '1' + fr
            transitions[new_fr][ch] = transitions[new_fr][ch].union(('1' + t for t in to))

      return Automata(states, unwrap_transitions(transitions), '0', {'1',})

   def orCombine(self, other):
      states = {'0', '1'}
      transitions = ddict(lambda : ddict(set))

      states = states.union(('0' + s for s in self._states))
      states = states.union(('1' + s for s in other._states))
      
      transitions['0'][''].add('0' + self._start)
      transitions['0'][''].add('1' + other._start)

      for term in self._term:
         transitions['0' + term][''].add('1')

      for term in other._term:
         transitions['1' + term][''].add('1')

      for fr, ways in self._transitions.items():
         for ch, to in ways.items():
            new_fr = '0' + fr
            transitions[new_fr][ch] = transitions[new_fr][ch].union(('0' + t for t in to))

      for fr, ways in other._transitions.items():
         for ch, to in ways.items():
            new_fr = '1' + fr
            transitions[new_fr][ch] = transitions[new_fr][ch].union(('1' + t for t in to))

      return Automata(states, unwrap_transitions(transitions), '0', {'1',})

   def _findAvailable(self, state, component):
      if not state in self._transitions:
         return
      
      for ch, to in self._transitions[state].items():
         if ch == '':
            for s in to:
               if not s in component:
                  component.add(s)
                  self._findAvailable(s, component)
   
   def _addTerm(self):
      for fr, ways in self._transitions.items():
         for ch, to in ways.items():
            if ch == '':
               for s in to:
                  if s in self._term:
                     self._term.add(fr)

   def _addTransitions(self):
      for fr, ways in self._transitions.items():
         for ch, to in ways.items():
            if ch == '':
               for s in to:
                  if not s in self._transitions:
                     continue
                  for ch2, to2 in self._transitions[s].items():
                     if ch2 != '':
                        for s2 in to2:
                           if ch2 in self._transitions[fr]:
                              self._transitions[fr][ch2].add(s2)
                           else:
                              self._transitions[fr][ch2] = {s2,}

   def _removeEps(self):
      for fr, ways in self._transitions.items():
         ways.pop('', None)

   def _epsClosure(self):
      #1
      for s in self._states:
         component = set()
         self._findAvailable(s, component)
         for avail in component:
            self._transitions[s][''].add(avail)

      #2
      self._addTerm()
      #3
      self._addTransitions()    
      #4
      self._removeEps() 


   def makeDsa(self):
      self._epsClosure()

      alphabet = set()
      for fr, ways in self._transitions.items():
         for ch, to in ways.items():
            alphabet.add(ch)

      queue = deque()
      queue.appendleft(('0', {self._start,}))

      states = set()
      next_state_id = '1'

      transitions = ddict(lambda : ddict(set))

      term = set()
      if self._start in self._term:
         term.add('0')

      visited_sets = {}

      while len(queue) != 0:
         idx, q = queue.pop()
         states.add(next_state_id)
         for ch in alphabet:
            next_q = set()
            for fr in q:
               if not fr in self._transitions or not ch in self._transitions[fr]:
                  continue
               next_q = next_q.union(self._transitions[fr][ch])

            if len(next_q) == 0:
               continue
            
            q_hash = frozenset(next_q).__hash__()
            if q_hash in visited_sets:
               transitions[idx][ch].add(visited_sets[q_hash])
               continue
            else:
               visited_sets[q_hash] = next_state_id
               
            transitions[idx][ch].add(next_state_id)

            queue.appendleft((next_state_id, next_q))
            for t in self._term:
               if t in next_q:
                  term.add(next_state_id)
            next_state_id = str(int(next_state_id) + 1)

      self._states = states
      self._transitions = unwrap_transitions(transitions)
      self._start = '0'
      self._term = term


   def apply(self, inp):
      curr = self._start
      for ch in inp:
         if curr not in self._transitions or ch not in self._transitions[curr]:
            return False
         curr = list(self._transitions[curr][ch])[0]

      return curr in self._term

def main():
   states = {'1', '0', '01', '00'}
   transitions = {'1': {'': set(['0'])}, '0': {'': set(['1', '00'])}, '00': {'a': set(['01'])}, '01': {'': set(['1'])}}
   a = Automata(states, transitions, '0', {'1',})

   a.makeDsa()
   print a
   print a.apply('')

if __name__ == '__main__':
   main()

