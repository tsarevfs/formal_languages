from itertools import product
from regexp2automata import regexp2automata
import re

def main():
	tests = [ "a?(b(a*)|c)*c"
			, "a*b*((ac)?|ab)"
			, "a(a|b|c)*b"
			, "a?b?(a*|b*)"
			, "a*(b*|c)?"
			, "b(ab)*c?"
			, "ba((ab)|(ac))*"
			, "((a|b)*|(b|c)*)?"
			, "(abc)*(a|b)*"
			, "(a*|b*)|c*"
			, "((ab)|(bc))*"
			, "(a?b?)*|c*"
			, "((ab)?|bc*|ac)"
			, "(a|b)*(abc)*"
			, "a*b*(ab|c)?"
			, "(a?|b?)*"
			, "a(ab)*b(bc)*"
			, "ac*ba*cb*"
			, "(a*|b)*|(b|c)*"
			, "(ab|c)*(a*|b)*"
			, "((a?b?)|(bc))*"
			, "((ab)?|cb*|ca)"
			, "(a|b|c)*(a|b)*"]

	#tests = ["a|b"]

	for test in tests:
		pattern = re.compile('^(' + test + ')$')
		automata = regexp2automata(test)
		errors = 0
		for i in range(10):
			for test_inp in (''.join(t) for t in product('abc', repeat=i)):
				right = pattern.match(test_inp) is not None
				my = automata.apply(test_inp)
				if my != right:
					errors += 1
					print automata
					print test, test_inp, right,  my

		print 'finished with %s errors' % str(errors)


if __name__ == '__main__':
	main()