from automata import *
from pyparsing import *
import traceback

def chFn(s, l, t):
    states = ['0', '1']
    transitions = {'0' : {t[0] : {'1',}}}
    return Automata(states, transitions, '0', {'1',})

def orFn(s, l, t):
    return reduce(lambda l, r : l.orCombine(r), t[0])

def closFn(s, l, t):
    return t[0][0].closure()

def optFn(s, l, t):
    return t[0][0].optional()

def concatFn(s, l, t):
    return reduce(lambda l, r : l.concat(r), t[0])

def regexp2automata(regexp):
    operand = Word(alphas, max=1).setParseAction(chFn)

    expr = operatorPrecedence( operand,
    [
     (Literal("?").suppress(), 1, opAssoc.LEFT, optFn),
     (Literal("*").suppress(), 1, opAssoc.LEFT, closFn),
     (None, 2, opAssoc.LEFT, concatFn), 
     (Literal("|").suppress(), 2, opAssoc.LEFT, orFn),
    ])

    grammar = expr

    ans = grammar.parseString(' '.join(regexp))[0]
    ans.makeDsa()
    return ans

def main():
    print regexp2automata("a|b")


if __name__ == '__main__':
    main()