from collections import defaultdict as ddict
from itertools import chain, product, compress
from copy import deepcopy

def str2sequence(str_repr):
    right_part = []
    terms = set()
    unterms = set()
    if (str_repr == '\\eps'):
        return (), terms, unterms
    i = 0
    for c in str_repr:
        right_part.append(c)
        if c.isupper():
            unterms.add(c)
        else:
            terms.add(c)
    return tuple(right_part), terms, unterms


class Grammatics:
    def __init__(self, terms, unterms, rules):
        self._terms = terms
        self._unterms = unterms
        self._rules = rules

    @staticmethod
    def from_string(data):
        terms = set()
        unterms = set()
        rules = ddict(set)
        for s_rule in data.split('\n'):
            left_part, sep, tail = s_rule.rpartition('->')
            for rule_part in tail.split('|'):
                right_part, rpart_terms, rpart_unterms = str2sequence(rule_part)
                terms = terms.union(rpart_terms)
                unterms = unterms.union(rpart_unterms)
                unterms.add(left_part)

                rules[left_part].add(right_part)

        return Grammatics(terms, unterms, dict(rules))

    def __repr__(self):
        rules_texts = []
        for lp, rps in self._rules.items():
            rp_texts = []
            for rp in rps:
                rp_texts.append('\\varepsilon' if len(rp) == 0 else ''.join(rp))
            
            rules_texts.append(lp + ' &\\to ' + ' \\mid '.join(rp_texts))

        terms_text = '$\\left \\{ %s \\right \\}$' % ', '.join((str(a) for a in self._terms))
        unterms_text = '$\\left \\{ %s \\right \\}$' % ', '.join((str(a) for a in self._unterms))

        return 'terms: %s\nunterms: %s\n\\begin{equation}\n  \\begin{aligned}\n%s\n  \\end{aligned}\n\\end{equation}\n' % (terms_text,
                                                       unterms_text,
                                                       '\\\\\n'.join(('    ' + s for s in rules_texts)))

    def _filter_in_set(self, S):
        filtered_rules = ddict(set)
        for lp, rps in self._rules.items():
            for rp in rps:
                if lp in S and all((ut not in self._unterms or ut in S for ut in rp)):
                    filtered_rules[lp].add(rp)

        self._rules = dict(filtered_rules)
        self._unterms = S

        new_terms = set()
        for lp, rps in self._rules.items():
            new_terms = new_terms.union(self._terms.intersection(chain(*rps)))

        self._terms = new_terms

    def filter_generating(self):
        S = set()
        for lp, rps in self._rules.items():
            for rp in rps:
                if len(set(rp).intersection(self._terms)) == 0:
                    S.add(lp)

        changes = True
        while changes:
            changes = False

            for lp, rps in self._rules.items():
                if lp in S:
                    continue
                for rp in rps:
                    if len(set(rp).intersection(S)) == len(set(rp).intersection(self._unterms)):
                        S.add(lp)
                        changes = True

        self._filter_in_set(S)


    def filter_reacheble(self):
        S = {'S',}
        changes = True
        while changes:
            changes = False

            for lp, rps in self._rules.items():
                if lp not in S:
                    continue
                
                newS = S.union(self._unterms.intersection(chain(*rps)))
                if (S != newS):
                    S = newS
                    changes = True


        self._filter_in_set(S)  

    def _find_eps_generating(self):
        S = set()                 
        for lp, rps in self._rules.items():
            for rp in rps:
                if len(rp) == 0:
                    S.add(lp)
        
        changes = True
        while changes:
            changes = False

            for lp, rps in self._rules.items():
                for rp in rps:
                    if lp not in S and all((ut in S for ut in rp)):
                        S.add(lp)
                        changes = True
        return S

    def filter_eps_rules(self):
        rules = deepcopy(self._rules)
        eps_unterms = self._find_eps_generating()
        for lp, rps in self._rules.items():
            for rp in rps:
                egu_idxs = set(i for i, e in enumerate(rp) if e in eps_unterms)

                if (len(egu_idxs) == 0):
                    continue

                for pr in product((0, 1), repeat=len(egu_idxs)):
                    take_eg = set(compress(egu_idxs, pr))
                    take = (1 if (i in take_eg or i not in egu_idxs) else 0 for i, _ in enumerate(rp))
                    rules[lp].add(tuple(compress(rp, take)))

        self._rules = ddict(set)
        for lp, rps in rules.items():
            for rp in rps:
                if len(rp) > 0:
                    self._rules[lp].add(rp)

        if 'S' in eps_unterms:
            self._rules['S'].add(())

    def filter_chain_rules(self):
        rev_cahin_pairs = {ut : {ut,} for ut in self._unterms}

        changes = True
        while changes:
            changes = False

            for lp, rps in self._rules.items():
                for rp in rps:
                    if len(rp) == 1 and rp[0] in self._unterms:
                        r = rev_cahin_pairs[rp[0]]
                        rev_cahin_pairs[rp[0]] = r.union(rev_cahin_pairs[lp])
                        if len(r) != len(rev_cahin_pairs[rp[0]]):
                            changes = True

        rules = ddict(set)
        for r, ls in rev_cahin_pairs.items():
            for l in ls:
                for rp in self._rules[r]:
                    if len(rp) > 1 or len(rp) == 1 and rp[0] not in self._unterms:
                        rules[l].add(rp)

        for lp, rps in self._rules.items():
            for rp in rps:
                if len(rp) != 1 or rp[0] not in self._unterms:
                    rules[lp].add(rp)

        self._rules = rules


#1.1, 2.4, 3.7, 4.2.