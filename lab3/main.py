from grammatics import *

def main():
    test = []
    test.append('\
S->A|B\n\
A->aB|bS|b\n\
B->AB|Ba|AS|b\n\
C->b\
')
    test.append('\
S->SS|1A0\n\
A->1A0|\\eps\n\
')
    test.append('\
S->S+T|T\n\
T->T&P|P\n\
P->~P|(S)|i\
')
    test.append('\
S->A|B\n\
C->S|a|\\eps\n\
A->C|D\n\
D->S|b\n\
B->D|E\n\
E->S|c|\\eps\
')
    for t in test:
	    gr = Grammatics.from_string(t)
	    print '--initial--'
	    print gr

	    gr.filter_eps_rules()
	    print '--eps--'
	    print gr

	    gr.filter_chain_rules()
	    print '--chain--'
	    print gr

	    gr.filter_generating()
	    print '--generating--'
	    print gr

	    gr.filter_reacheble()
	    print '--reacheble--'
	    print gr

if __name__ == '__main__':
    main()